import enviar_email
import database as db
import calendar
import relatorioRetorno as relRet

def processar_arquivo(envio,retorno):
    gambi = (('0202287351', '7'), ('0631410589', '7'), ('1012182891', '7'), ('1021482452', '7'), ('1052395101', '7'),
             ('1170836671', '7'), ('1579584010', '7'), ('5065287895', '7'), ('5082741310', '7'), ('1084562674', '7'),
             ('1122497030', '7'), ('1072172507', '7'), ('0800101006', '7'), ('0674001133', '7'), ('1192960278', '7'),
             ('1025674852', '7'), ('1332411379', '7'), ('1540881978', '7'), ('1332411255', '7'), ('1119936397', '7'),
             ('1085567661', '7'), ('1055290068', '7'), ('1046901661', '7'), ('1253461209', '7'), ('0207178674', '7'),
             ('1203674470', '7'), ('0599857455', '7'), ('1146460454', '7'), ('1156425635', '7'), ('1086343287', '7'),
             ('1286407165', '7'), ('0414617428', '7'), ('0734936729', '7'), ('1256603861', '7'), ('1083487660', '7'),
             ('5359106456', '7'), ('1012225035', '7'), ('1034604411', '7'), ('1149898191', '7'), ('5334370060', '7'),
             ('0203072049', '7'), ('1457324137', '7'), ('1176254933', '7'), ('1292850202', '7'), ('5199977152', '7'),
             ('0599196866', '7'), ('5206636548', '7'), ('1373845160', '7'), ('5517230594', '7'), ('1225086873', '7'),
             ('0670887773', '7'), ('1254717436', '7'), ('1580441766', '7'), ('0414676327', '7'), ('1186905007', '7'),
             ('0414551931', '7'), ('0674003314', '7'), ('0864451067', '7'), ('1115213668', '7'), ('0439594413', '7'),
             ('1073096928', '7'), ('0413749436', '7'), ('0728494051', '7'), ('1277873388', '7'), ('0439590698', '7'),
             ('1068453572', '7'), ('5538332124', '7'), ('1249434855', '7'), ('1319158444', '7'), ('1185069841', '7'),
             ('0414568451', '7'), ('1248466524', '7'), ('1269660710', '7'),)

    mes_ano = retorno[-6:]
    ano = mes_ano[:4]
    mes = mes_ano[-2:]

    # valida mes e ano anterios do arquivo de movimentacao e prepara data para o select
    mes_anterior = int(mes) - 1
    ano_anterior = ano
    if mes_anterior == 0:
        mes_anterior = "12"
        ano_anterior = str(int(ano) - 1)
    elif mes_anterior <= 9:
        mes_anterior = "0" + str(mes_anterior)
    else:
        mes_anterior = str(mes_anterior)

    ultimo_dia = calendar.monthrange(int(ano_anterior), (int(mes_anterior)))
    date_filtro_bd = ano_anterior + "-" + mes_anterior + "-" + str(ultimo_dia[1])

    # executa o select
    select = "SELECT numerobeneficio,status FROM [tbl_socio] where ((status = '00') or (status = '04') or (status = '13' and datareativacao <= '" + date_filtro_bd + " 23:59:59') or (status = '09' and datainclusao <= '" + date_filtro_bd + " 23:59:59') or (status = '11') or (status = '25') or (status = '27') or(status = '28')) and numerobeneficio <> '' ;"
    rows = db.select_bdanapps(select)

    # arquivo enviado a dataprev
    file_enviado = 'dataprev/'+envio
    arquivo = open(file_enviado, 'r', encoding="ansi")
    # arquivo de retorno
    file_retorno = 'dataprev/'+retorno
    arquivo2 = open(file_retorno, 'r', encoding="ansi")

    # arquivos divididos
    arquivoAnapps = open('dataprev/anapps/' + retorno, 'w+', encoding="ansi")
    arquivoBmg = open('dataprev/bmg/' + retorno+".dat", 'w+', encoding="ansi", newline='\n')

    cont = 0
    t = ()
    # print(arquivo)
    for line in arquivo:
        cont += 1
        cod = line[:1]
        nb = line[1:11]
        op = line[11:12]
        nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]

        t = t + ((nbFormatado, op),)

    header = True
    cont1 = 0
    cont2 = 0
    for line in arquivo2:
        if len(line.replace(" ", "")) < 30:
            if header:
                print("Header")
                arquivoAnapps.write(line)
                arquivoBmg.write(line)
                header = False
            else:
                auxbd = ""
                for x in range(len(str(cont1)), 6):
                    auxbd = auxbd + "0"
                auxbd = auxbd + str(cont1)

                auxbd2 = ""
                for x in range(len(str(cont2)), 6):
                    auxbd2 = auxbd2 + "0"
                auxbd2 = auxbd2 + str(cont2)

                arquivoAnapps.write("9" + auxbd + "                                      ")
                arquivoBmg.write("9" + auxbd2 + "                                      ")
                print("Trailer")
        else:
            nb = line[1:11]
            op = line[11:12]
            codigo = line[13:16]
            # print(codigo)
            nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]
            print(nbFormatado)
            if [tup for tup in t if nbFormatado in tup[0] and op == tup[1]]:
                # print(nbFormatado)
                arquivoAnapps.write(line)
                cont1 = cont1 + 1
            else:
                # print(nbFormatado)
                # print(rows)
                if [tup2 for tup2 in rows if nbFormatado in tup2[0] and tup2[1] != '04']:
                    arquivoAnapps.write(line)
                    cont1 = cont1 + 1
                else:
                    if op == '7' and codigo == '102':
                        if [tup2 for tup2 in rows if nbFormatado in tup2[0] and tup2[1] == '04']:
                            print(nbFormatado)
                            arquivoAnapps.write(line)
                            cont1 = cont1 + 1
                        else:
                            arquivoBmg.write(line)
                            cont2 = cont2 + 1
                    else:
                        if [tup3 for tup3 in gambi if nb in tup3[0]]:
                            arquivoAnapps.write(line)
                            cont1 = cont1 + 1
                        else:
                            arquivoBmg.write(line)
                            cont2 = cont2 + 1
    print(str(cont1))
    print(str(cont2))
    arquivoAnapps.close()
    arquivoBmg.close()
    relRet.gerarRelatorio(retorno)
