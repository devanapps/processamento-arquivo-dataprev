import smtplib
from email import message
from os.path import basename
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

def enviar_email(email,cc,arquivos):
    port = 587  # For starttls
    smtp_server = "smtp.anapps.org.br"
    password = "#####"

    email_auth = "diego.bernardes@anapps.org.br"
    from_addr = "Diego Bernardes <diego.bernardes@anapps.org.br>"
    to_addr = email
    to_addr_list = ""
    for x in email:
        if to_addr_list == "":
            to_addr_list = x+";"
        else:
            to_addr_list = to_addr_list+x+";"
    to_cc = cc
    subject = 'Arquivo dataprev'
    content = 'Arquivo processado'
    html = """\
    <html>
      <body>
        <p>Prezados,<br>
           Segue em anexo os arquivos divididos.<br>
           O arquivo com final ".dat" é destinado ao BMG.<br><br>
           Atenciosamente,<br>
        </p>
        <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="1258" style="width:943.85pt"><tbody><tr style="height:13.5pt"><td width="155" rowspan="4" style="width:116.25pt;padding:0cm 0cm 0cm 0cm;height:13.5pt"><p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#1F497D;mso-fareast-language:PT-BR"><img border="0" width="150" height="88" style="width:1.5666in;height:.9166in" id="_x0000_i1034" src="http://anapps.org.br/anapps.png" alt="http://anapps.org.br/anapps.png"><o:p></o:p></span></p></td><td width="1094" style="width:820.4pt;padding:0cm 0cm 0cm 0cm;height:13.5pt"><p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#385623;mso-fareast-language:PT-BR"><o:p>&nbsp;</o:p></span></b></p><p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#385623;mso-fareast-language:PT-BR">Diego Bernardes<o:p></o:p></span></b></p></td></tr><tr style="height:13.5pt"><td width="1094" style="width:820.4pt;padding:0cm 0cm 0cm 0cm;height:13.5pt"><p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#385623;mso-fareast-language:PT-BR">Tecnologia da Informação<o:p></o:p></span></p></td></tr><tr style="height:13.5pt"><td width="1094" style="width:820.4pt;padding:0cm 0cm 0cm 0cm;height:13.5pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#385623;mso-fareast-language:PT-BR">Telefone: (51) 3119-3020<o:p></o:p></span></p></td></tr><tr style="height:13.5pt"><td width="1094" style="width:820.4pt;padding:0cm 0cm 0cm 0cm;height:13.5pt"><p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,sans-serif;color:#385623;mso-fareast-language:PT-BR"><a target="_blank" onclick="top.verifyExternalLink('.modal-external-link', 'http://www.anapps.org.br/', event );" xhref="http://www.anapps.org.br/"><span style="color:#385623">www.anapps.org.br</span></a><o:p></o:p></span></p></td></tr></tbody></table>
      </body>
    </html>
    """

    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = to_addr_list
    msg['Subject'] = subject
    body = MIMEText(html, 'html')
    msg.attach(body)

    #filenames = ['dataprev/anapps/'+arquivo,'dataprev/bmg/'+arquivo+'.dat']
    for filename in arquivos:
        print(filename)
        with open(filename, 'r') as f:
            part = MIMEApplication(f.read(), Name=basename(filename))
        part['Content-Disposition'] = 'attachment; filename="{}"'.format(basename(filename))
        msg.attach(part)

    server = smtplib.SMTP(smtp_server, port)
    server.login(email_auth, password)
    server.send_message(msg, from_addr=from_addr, to_addrs=to_addr)

#enviar_email(["diego.bernardes@anapps.org.br","diegob.bernardes@gmail.com"],"","D.SUB.GER.102.REP.201903")

#arquivos = ['dataprev/anapps/D.SUB.GER.102.REP.201904', 'dataprev/bmg/D.SUB.GER.102.REP.201904.dat','dataprev/anapps/D.SUB.GER.102.201903','dataprev/bmg/D.SUB.GER.102.201903.dat']
#enviar_email(["diego.bernardes@anapps.org.br","mateus.bastos@anapps.org.br"],"",arquivos)
