def numeroMes(argument):
    switcher = {
        1: um,
        2: dois,
        3: tres,
        4: quatro,
        5: cinco,
        6: seis,
        7: sete,
        8: oito,
        9: nove,
        10: dez,
        11: onze,
        12: doze
    }

    try:
        argument = int(argument)
    except ValueError as verr:
        pass  # do job to handle: s does not contain anything convertible to int
    except Exception as ex:
        pass  # do job to handle: Exception occurred while converting to int

    func = switcher.get(argument, lambda: "invalido")
    return func()

def um():
    return "Janeiro"

def dois():
    return "Fevereiro"

def tres():
    return "Março"

def quatro():
    return "Abril"

def cinco():
    return "Maio"

def seis():
    return "Junho"

def sete():
    return "Julho"

def oito():
    return "Agosto"

def nove():
    return "Setembro"

def dez():
    return "Outubro"

def onze():
    return "Novembro"

def doze():
    return "Dezembro"
