import os, fnmatch, shutil
import processamento_repasse as pr
import enviar_email

files = 0
arquivo_repasse = ''

while True:
    lista_de_arquivos = os.listdir('processamento/.')
    #validacao de arquivos dataprev
    pattern = ["*.102.*", "*.101.*"]
    for arquivo in lista_de_arquivos:
        if(fnmatch.fnmatch(arquivo, pattern[0])) or (fnmatch.fnmatch(arquivo, pattern[1])):
            #verifica se o arquivo e o repasse do mes
            if(fnmatch.fnmatch(arquivo, '*.102.REP.*')):
                arquivo_repasse = arquivo
            #move o arquivo da pasta processamento para a pasta dataprev
            shutil.move("processamento/"+arquivo, "dataprev/")
            files = files + 1
            # valida o numero de arquivos quando chegar a 3 seta para 0 e executa a funcao de processamento
            if files == 3:
                print("executando")
                files = 0
                arquivos = pr.processar_arquivo(arquivo_repasse)
                enviar_email.enviar_email(["diego.bernardes@anapps.org.br", "mateus.bastos@anapps.org.br"], "",
                                          arquivos)