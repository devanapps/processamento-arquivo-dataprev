import xlsxwriter
import datas as dt

def gerarXls(arquivo,pasta,workbook):

    file = "dataprev/" + pasta + "/" + arquivo
    arquivo = open(file, 'r', encoding="utf-8")

    worksheet = workbook.add_worksheet(pasta)

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})

    # Add a number format for cells with money.
    money = workbook.add_format({'num_format': 'R$ #,##.00'})

    col = 0
    row = 0
    registros = 0
    arquivo.__next__()

    # variaveis de resumo
    inclusaoOk = 0
    inclusaoErro = 0
    exclusaoOk = 0
    exclusaoErro = 0
    movimentacaoAuto = 0

    data = [
        ['001', "DV do NB inválido", 0],
        ['002', "Espécie incompatível", 0],
        ['003', "Ramo atividade incompatível", 0],
        ['004', "NB inexistente no cadastro", 0],
        ['005', "Benefício não ativo", 0],
        ['006', "Valor ultrapassa MR do titular", 0],
        ['007', "Já existe desconto p/ outra entidade", 0],
        ['008', "Já existe desconto p/ Entidade", 0],
        ['010', "Já existe desconto p/ Entidade com percentual diferente", 0],
        ['999', "Outros erros", 0],
    ]

    dataExclusao = [
        ['001', "DV do NB inválido", 0],
        ['004', "NB inexistente no cadastro", 0],
        ['005', "Benefício não ativo", 0],
        ['009', "Consignação já excluída", 0],
        ['999', "Outros erros", 0],
    ]

    dataMovs = [
        ['101', "Suspensão desconto por susp. benefício", 0],
        ['102', "Reativação desconto por reativ. benefício", 0],
        ['103', "Suspensão desconto por óbito do titular", 0],
        ['104', "Suspensão desconto por solicitada via WEB", 0],
        ['105', "Reativação desconto por solicitada via WEB", 0],
    ]

    worksheet.write(row, 0, "Beneficio", bold)
    worksheet.write(row, 1, "Operacao", bold)
    worksheet.write(row, 2, "Resultado", bold)
    worksheet.write(row, 3, "Erro", bold)

    row += 1
    header = True
    for line in arquivo:
        if len(line.replace(" ", "")) < 26:
            if header:
                header = False
            else:
                print("footer")
        else:
            nb = line[1:11]
            codigoOperacao = line[11:12]
            codigoResultado = line[12:13]
            codigoErro = line[13:16]
            nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]

            if codigoOperacao == "1":
                if codigoResultado == "1":
                    inclusaoOk += 1
                elif codigoResultado == "2":
                    inclusaoErro += 1
                    if codigoErro == "001":
                        data[0][2] = data[0][2] + 1
                    elif codigoErro == "002":
                        data[1][2] = data[1][2] + 1
                    elif codigoErro == "003":
                        data[2][2] = data[2][2] + 1
                    elif codigoErro == "004":
                        data[3][2] = data[3][2] + 1
                    elif codigoErro == "005":
                        data[4][2] = data[4][2] + 1
                    elif codigoErro == "006":
                        data[5][2] = data[5][2] + 1
                    elif codigoErro == "007":
                        data[6][2] = data[6][2] + 1
                    elif codigoErro == "008":
                        data[7][2] = data[7][2] + 1
                    elif codigoErro == "010":
                        data[8][2] = data[8][2] + 1
                    elif codigoErro == "999":
                        data[9][2] = data[9][2] + 1
            elif codigoOperacao == "5":
                if codigoResultado == "1":
                    exclusaoOk += 1
                elif codigoResultado == "2":
                    exclusaoErro += 1
                    if codigoErro == "001":
                        dataExclusao[0][2] = dataExclusao[0][2] + 1
                    if codigoErro == "004":
                        dataExclusao[1][2] = dataExclusao[1][2] + 1
                    if codigoErro == "005":
                        dataExclusao[2][2] = dataExclusao[2][2] + 1
                    if codigoErro == "009":
                        dataExclusao[3][2] = dataExclusao[3][2] + 1
                    if codigoErro == "999":
                        dataExclusao[4][2] = dataExclusao[4][2] + 1
            elif codigoOperacao == "7":
                movimentacaoAuto += 1
                if codigoErro == "101":
                    dataMovs[0][2] = dataMovs[0][2] + 1
                if codigoErro == "102":
                    dataMovs[1][2] = dataMovs[1][2] + 1
                if codigoErro == "103":
                    dataMovs[2][2] = dataMovs[2][2] + 1
                if codigoErro == "104":
                    dataMovs[3][2] = dataMovs[3][2] + 1
                if codigoErro == "105":
                    dataMovs[4][2] = dataMovs[4][2] + 1
            else:
                print("ok")

            worksheet.write(row, col, nbFormatado)
            worksheet.write(row, col + 1, codigoOperacao)
            worksheet.write(row, col + 2, codigoResultado)
            worksheet.write(row, col + 3, codigoErro)

            row += 1

    row = 0

    dataResumo = [
        ['Inclusao Ok', inclusaoOk],
        ['Inclusao Erro', inclusaoErro],
        ['Exclusao Ok', exclusaoOk],
        ['Exclusao Erro', exclusaoErro],
        ['Movimentacao Auto', movimentacaoAuto],
    ]

    worksheet.add_table('F2:G7', {'columns': [{'header': 'Resumo'}, {'header': 'Quantidade'}], 'data': dataResumo,
                                  'style': 'Table Style Light 9', 'autofilter': False})

    worksheet.add_table('I1:K11', {'data': data, 'style': 'Table Style Light 9', 'autofilter': False,
                                   'columns': [{'header': 'Cod'}, {'header': 'Erros de inclusao'},
                                               {'header': 'Quantidade'}]})

    worksheet.add_table('I13:K18', {'data': dataExclusao, 'style': 'Table Style Light 9', 'autofilter': False,
                                    'columns': [{'header': 'Cod'}, {'header': 'Erros de exclusao'},
                                                {'header': 'Quantidade'}]})

    worksheet.add_table('I20:K25', {'data': dataMovs, 'style': 'Table Style Light 9', 'autofilter': False,
                                    'columns': [{'header': 'Cod'}, {'header': 'Movimentacoes automaticas'},
                                                {'header': 'Quantidade'}]})

def gerarRelatorio(arquivo):
    competencia = arquivo[-2:]
    nomeXls = 'dataprev/relatorios/retorno.dataprev.comp.' + dt.numeroMes(competencia) + '.xlsx'
    workbook = xlsxwriter.Workbook(nomeXls)

    gerarXls(arquivo, 'anapps', workbook)
    gerarXls(arquivo + ".dat", 'bmg', workbook)

    workbook.close()
    return nomeXls

#gerarRelatorio("D.SUB.GER.102.201903")