import xlsxwriter
import datas as dt

def gerarXls(arquivo,pasta,workbook):
    print(arquivo,pasta)

    file = "dataprev/" + pasta + "/" + arquivo
    arquivo = open(file, 'r', encoding="utf-8")

    worksheet = workbook.add_worksheet(pasta)

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})

    # Add a number format for cells with money.
    money = workbook.add_format({'num_format': 'R$ #,##.00'})

    col = 0
    row = 0
    registros = 0
    arquivo.__next__()
    valorTotal = 0

    worksheet.write(row, 0, "Beneficio", bold)
    worksheet.write(row, 1, "Competencia", bold)
    worksheet.write(row, 2, "Valor Desconto", bold)

    row += 1
    header = True
    for line in arquivo:
        if len(line.replace(" ", "")) < 26:
            if header:
                header = False
            else:
                print("footer")
        else:
            nb = line[1:11]
            competencia = line[11:13] + "/" + line[13:17]
            valor = line[21:26]

            nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]
            valorFormatado = float(valor[:3] + "." + valor[3:5])

            worksheet.write(row, col, nbFormatado)
            worksheet.write(row, col + 1, competencia)
            worksheet.write(row, col + 2, valorFormatado)

            row += 1
            registros += 1
            valorTotal += valorFormatado

    # criar o resumo do relatorio
    row = 0
    worksheet.write(row, 4, "Resumo", bold)
    worksheet.write(row + 1, 4, "Total de Registros")
    worksheet.write(row + 1, 5, registros)
    worksheet.write(row + 2, 4, "Total de Desconto")
    worksheet.write(row + 2, 5, valorTotal, money)

def gerarRelatorio(arquivo):
    competencia = arquivo[-2:]
    nomeXls = 'dataprev/relatorios/repasse.dataprev.comp.' + dt.numeroMes(competencia) + '.xlsx'
    workbook = xlsxwriter.Workbook(nomeXls)

    gerarXls(arquivo,'anapps',workbook)
    gerarXls(arquivo+".dat", 'bmg',workbook)

    workbook.close()
    return nomeXls

#gerarRelatorio("D.SUB.GER.102.REP.201904")
