import calendar
import database as db
import processamento_retorno as pr
import relatorioRepasse as relRep

def processar_arquivo(file):

    #carrega mes e ano
    mes_ano = file[-6:]
    ano = mes_ano[:4]
    mes = mes_ano[-2:]

    #valida mes e ano anterios do arquivo de movimentacao e prepara data para o select
    mes_anterior = int(mes)-1
    ano_anterior = ano
    if mes_anterior == 0:
        mes_anterior = "12"
        ano_anterior = str(int(ano)-1)
    elif mes_anterior <= 9:
        mes_anterior = "0" + str(mes_anterior)
    else:
        mes_anterior = str(mes_anterior)

    arquivo_envio = "D.SUB.GER.101."+str(ano_anterior)+str(mes_anterior)
    arquivo_retorno = "D.SUB.GER.102."+str(ano_anterior)+str(mes_anterior)
    pr.processar_arquivo(arquivo_envio, arquivo_retorno)

    ultimo_dia = calendar.monthrange(int(ano_anterior),(int(mes_anterior)))
    date_filtro_bd = ano_anterior+"-"+mes_anterior+"-"+str(ultimo_dia[1])

    #executa o select
    select = "SELECT numerobeneficio,status FROM [tbl_socio] where ((status = '00') or (status = '04') or (status = '13' and datareativacao <= '"+date_filtro_bd+" 23:59:59') or (status = '09' and datainclusao <= '"+date_filtro_bd+" 23:59:59') or (status = '11') or (status = '25') or (status = '27') or(status = '28')) and numerobeneficio <> '' ;"
    rows = db.select_bdanapps(select)


    #print(date_filtro_bd)
    #print(rows)

    #prepara os arquivos para processamento
    file_mov = 'D.SUB.GER.102.'+ano_anterior+mes_anterior
    arquivo_movimentacao = open("dataprev/"+file_mov, 'r', encoding="ansi")
    arquivo_repasse = open("dataprev/"+file, 'r', encoding="ansi")
    arquivo_anapps = open('dataprev/anapps/' + file, 'w+', encoding="ansi")
    arquivo_bmg = open('dataprev/bmg/' + file + ".dat", 'w+', encoding="ansi", newline='\n')

    movimentacao = ()
    # arquivo de movimentacao para uma tupla
    for line in arquivo_movimentacao:
        if len(line.replace(" ", "")) > 26:
            nb = line[1:11]
            op = line[11:12]
            codigo = line[13:16]
            nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]

            movimentacao = movimentacao + ((nbFormatado, op, codigo),)
    #print(movimentacao)

    #variaveis de validacao totais dos arquivos
    header = True
    anapps_total = 0
    anapps_valor_total = 0.0
    bmg_total = 0
    bmg_valor_total = 0.0

    #inicia validacao arquivo repasse

    for line in arquivo_repasse:
        if len(line.replace(" ", "")) < 26:
            if header:
                print("Header")

                header = False
                #adiciona a linha Header
                arquivo_anapps.write(line)
                arquivo_bmg.write(line)
            else:
                print("footer")

                #valida numero de registros
                aux_anapps_total = ""
                for x in range(len(str(anapps_total)), 8):
                    aux_anapps_total = aux_anapps_total + "0"
                aux_anapps_total = aux_anapps_total + str(anapps_total)

                aux_bmg_total = ""
                for x in range(len(str(bmg_total)), 8):
                    aux_bmg_total = aux_bmg_total + "0"
                aux_bmg_total = aux_bmg_total + str(bmg_total)

                #valida valores
                aux_anapps_valor = ""
                for x in range(len(str(("%.2f" %anapps_valor_total)).replace(".", "")), 11):
                    aux_anapps_valor = aux_anapps_valor + "0"
                aux_anapps_valor = aux_anapps_valor + str(("%.2f" %anapps_valor_total)).replace(".", "")

                aux_bmg_valor = ""
                for x in range(len(str(("%.2f" % bmg_valor_total)).replace(".", "")), 11):
                    aux_bmg_valor = aux_bmg_valor + "0"
                aux_bmg_valor = aux_bmg_valor + str(("%.2f" % bmg_valor_total)).replace(".", "")

                #adiciona a linha Trailer
                arquivo_anapps.write("9"+aux_anapps_total+aux_anapps_valor+"      ")
                arquivo_bmg.write("9"+aux_bmg_total+aux_bmg_valor+"      ")
        else:
            tipoResgistro = line[:1]
            nb = line[1:11]
            competenciaDesconto = line[11:17]
            especie = line[17:19]
            uf = line[19:21]
            valorDesconto = line[21:26]
            valorFormatado = float(valorDesconto[:3] + "." + valorDesconto[3:5])
            nbFormatado = nb[:3] + "." + nb[3:6] + "." + nb[6:9] + "-" + nb[9:10]
            print(nbFormatado)

            if [tup for tup in rows if nbFormatado in tup[0] and tup[1] != '04']:
                anapps_total = anapps_total + 1
                anapps_valor_total = anapps_valor_total + valorFormatado
                # copia a linha do arquivo para o arquivo anapps
                arquivo_anapps.write(line)
            else:
                # veirifca se e reativacao automatica codigo 102
                if [tup for tup in movimentacao if nbFormatado in tup[0] and tup[1] == '7' and tup[2] == '102']:
                    if [tup for tup in rows if nbFormatado in tup[0] and tup[1] == '04']:
                        anapps_total = anapps_total + 1
                        anapps_valor_total = anapps_valor_total + valorFormatado
                        arquivo_anapps.write(line)
                    else:
                        bmg_total = bmg_total + 1
                        bmg_valor_total = bmg_valor_total + valorFormatado
                        arquivo_bmg.write(line)
                else:
                    # copia a linha do arquivo para o arquivo bmg
                    bmg_total = bmg_total + 1
                    bmg_valor_total = bmg_valor_total + valorFormatado
                    arquivo_bmg.write(line)

    print("anapps:"+str(anapps_total))
    print("BMG:"+str(bmg_total))

    arquivo_anapps.close()
    arquivo_bmg.close()
    arquivoRelatorio = relRep.gerarRelatorio(file)
    #arquivos = [arquivoRelatorio,'dataprev/anapps/'+file,'dataprev/anapps/'+file+".dat"]
    arquivos = ['dataprev/anapps/' + file, 'dataprev/bmg/' + file + ".dat",'dataprev/anapps/' + arquivo_retorno,'dataprev/bmg/' + arquivo_retorno + ".dat"]
    return arquivos
